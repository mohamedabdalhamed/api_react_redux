<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login','PassportController@login');
Route::post('register','PassportController@register');

Route::middleware('auth:api')->group( function () {
Route::get('user','PassportController@details');
Route::resource('course','apiCoursesController');
Route::resource('categoery','apicategoery');
Route::get('sreach/{sreach}','apicategoery@sreach');
Route::get('getall','apicategoery@all');
Route::get('getall/{orders}','apicategoery@orders');
Route::get('fav/{id}','otherController@fav');
Route::get('unfav/{id}','otherController@unfav');
Route::get('showfav/{id}','otherController@show');
Route::post('addcomment','otherController@Addcomments');
Route::get('comments/{id}','otherController@ShowComment');
Route::get('showlike','otherController@Showlike');
Route::get('DeleteComment/{id}','otherController@deletecoment');
Route::get('showcosw/{id}','apiCoursesController@showcosw');
Route::get('soin/{id}','apiCoursesController@Join');
Route::get('subCatgeorys/{id}','api\subCatgeory@index')->name('subCatgeory');
Route::get('subCatgeory/{id}','api\subCatgeory@show')->name('subCatgeoryshow');
Route::get('getCourse/{id}','api\subCatgeory@getCourse')->name('getCourse');
Route::get('getCoursevideo/{id}','api\subCatgeory@getCoursevideo')->name('getCoursevideo');
Route::get('getvideo/{id}','api\subCatgeory@getvideo')->name('getvideo');
Route::get('alluser','api\subCatgeory@alluser')->name('alluser');
Route::get('getusers/{id}','otherController@getuser')->name('getusers');
Route::post('catgoeryapi','otherController@catgoeryapi')->name('catgoeryapi');
Route::get('getposts','otherController@getposts')->name('getposts');
Route::delete('deletcat/{id}','otherController@destroys')->name('deletcat');
Route::get('edits/{id}','otherController@editcat')->name('edits');
Route::post('editsend/{id}','otherController@sendedit')->name('editsend');
Route::post('subcate','otherController@subcate')->name('subcate');
Route::get('showcate','otherController@showcate')->name('showcate');
Route::delete('deletesubcat/{id}','otherController@deletesubcat')->name('deletesubcat');
Route::get('editsubcate/{id}','otherController@editsubcate')->name('editsubcate');
Route::post('sendeditsubcate/{id}','otherController@sendeditsubcate')->name('sendeditsubcate');
Route::post('courese','otherController@courese')->name('courese');
Route::get('showcourese','otherController@showcourese')->name('showcourese');
Route::delete('destroyscourses/{id}','otherController@destroyscourses')->name('destroyscourses');
Route::get('editcourse/{id}','otherController@editcourse')->name('destroyscourses');
Route::get('roles/{id}','otherController@roles')->name('roles');
Route::post('updateuser/{id}','otherController@updateuser')->name('updateuser');
Route::get('Rate/{id}','RateandFollow@Rate')->name('Rate');






});
