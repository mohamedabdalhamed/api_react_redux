@extends('layouts.appbackend')

@section('content')
<div class="container-fluid">
@if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        <img src="uploads/{{ Session::get('file') }}">
        @endif
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <a  class="btn btn-info text-white my-3" href="{{route('catgoerys.create')}}">Add new catgoerys</a>
<table class="table table-striped">
<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">name</th>
      <th scope="col">img</th>
      <th scope="col">describtion</th>

    </tr>
  </thead>
  <tbody>
      @foreach($catgoery as $cate)
      <tr>
    
      <th scope="row">{{$cate->id}}</th> 
      <th scope="row">{{$cate->nameCatgorey}}</th> 

      <th scope="row"><img src="{{url('uploads/catge/',$cate->img)}}" > </th> 
      <th scope="row">{{$cate->nameCatgorey}}</th> 
    </tr>
      @endforeach
      
</tbody>
</table>
</div>
@endsection
