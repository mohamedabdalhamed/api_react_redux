@extends('layouts.appbackend')

@section('content')
<div class="container-fluid">
@if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        <img src="uploads/{{ Session::get('file') }}">
        @endif
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <a  class="btn btn-info text-white my-3" href="{{route('subcategoerys.create')}}">Add new sub Categoery</a>
<table class="table table-striped">
<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">name</th>
      <th scope="col">img</th>


    </tr>
  </thead>
  <tbody>
      @foreach($subcategoery as $as)
      <tr>
    
      <th scope="row">{{$as->id}}</th> 
      <th scope="row">{{$as->name}}</th> 
      <td><img src="{{url('uploads/subCateg',$as->imges)}}"   class="img-thumbnail" width="100"> </td>

      </tr>

      @endforeach
      
</tbody>
</table>
</div>
@endsection
