@extends('layouts.appbackend')

@section('content')
<div class="container">

<form method="post" action="{{route('course.store')}}" enctype="multipart/form-data">
            @csrf
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="name">
  </div>

  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">img</label>
    <input type="file" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="img">
  </div>

<!-- 
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">video</label>
    <input type="file" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="video[]">

  </div> -->

  <div class="input-group hdtuto control-group lst increment" >
      <input type="file" name="video[]" class="myfrm form-control">
      <div class="input-group-btn"> 
        <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
      </div>
    </div>
    <div class="clone hide">
      <div class="hdtuto control-group lst input-group" style="margin-top:10px">
        <input type="file" name="video[]" class="myfrm form-control"multiple >
        <div class="input-group-btn"> 
          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
        </div>
      </div>
    </div>


  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">categoery</label>
    <select name="catgeory" class="form-control">
     @foreach($catgoery as $cat)
     <option value={{$cat->id}}>{{$cat->name}}</option>
     @endforeach

</select>
  </div>

  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">number of video</label>
    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="numvideo">
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>


  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">describtion</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"name="desc">
  </div>

  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">age</label>
    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="age">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

</table>
</div>




<script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".hdtuto control-group lst").remove();
      });
    });
</script>
@endsection
