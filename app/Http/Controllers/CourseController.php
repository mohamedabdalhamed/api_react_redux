<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\couses;
use App\subcategoery;
use App\videos;
class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $couses=couses::all();
        return view('courses.courses',compact('couses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $catgoery=subcategoery::all();
        return view('courses.create',compact('catgoery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $couses=new couses();
        // $request->validate([
        //     'file' => 'required|mimes:pdf,xlx,csv|max:2048',
        // ]);
        $couses->name=$request->name;

  
        $imgname = time().'.'.$request->img->extension();  
   
        $request->img->move(public_path('uploads'), $imgname);
        
     

           $couses->imges=$imgname;
           $couses->videos='';
           $couses->numberOfVideo=$request->numvideo;
           $couses->describtion=$request->desc;           
           $couses->age=$request->age;
           $couses->category=$request->catgeory;
           
           $couses->save();
        
           if($request->hasfile('video'))
           {

$i=0;            foreach($request->file('video') as $file)
              {
     
                $videos=new videos();

                $name = $i . $file->hashName();
                  $file->move(public_path().'/uploads/videos/', $name);  
                  $videos->couse_id=$couses->id;;
                  $videos->video=$name;
                  
                  $videos->Save();
     
     $i++;           }
     
            }    
           


        return back()
            ->with('success','You have successfully upload file.')
            ->with('file',$imgname);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
