<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\subCategoery;
use App\catgoery;

class subcategoerys extends Controller
{
    /**
     * Display a listing of the resource.
     *subCategoery
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategoery=subcategoery::all();
        return view('SubCategoery.subCategoery',compact('subcategoery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $catgoery=catgoery::all();
        return view('SubCategoery.create',compact('catgoery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $catgoery=new subCategoery();
        $catgoery->name=$request->name;
        $imgname = time().'.'.$request->img->extension();
        $request->img->move(public_path('uploads/subCateg'), $imgname);
        $catgoery->imges=$imgname;
        $catgoery->numberCategoery_id=$request->catgeory;

        $catgoery->save();

        return back()
            ->with('success','You have successfully upload file.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
