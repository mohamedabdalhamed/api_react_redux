<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catgoery;

class apicategoery extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catgoery=catgoery::all()->take(4);
        return response()->json(['catgoery'=>$catgoery,'message'=>'success'],200);
    }
    public function all()
    {
        $catgoery=catgoery::paginate(9);
        return response(['catgoery'=>$catgoery,'message'=>'success'],200);
    }
    public function orders($order)
    {
        if($order=="old"){
        $catgoery=catgoery::where('Status',1)->orderBy('id','DESC')->paginate(9);
        }else{
            $catgoery=catgoery::where('Status',1)->orderBy('id','ASC')->paginate(9);

        }
        return response(['catgoery'=>$catgoery,'message'=>'success'],200);
    }

    public function sreach($sreach)
    {
        $catgoery=catgoery::where('nameCatgorey', 'LIKE', "%{$sreach}%")->get();
        return response()->json(['catgoery'=>$catgoery,'message'=>'success'],200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $catgoery=catgoery::find($id);
    return response()->json(['catgoery'=>$catgoery,'message'=>'success'],200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
