<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catgoery;
use Session;

class categoery extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
    }  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catgoery=catgoery::all();
        return view('Catgoery.catgoery',compact('catgoery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Catgoery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $catgoery=new catgoery();
        $catgoery->nameCatgorey=$request->nameCatgorey;
        $catgoery->start=$request->start;
        $catgoery->end=$request->end;
        $catgoery->dsceribtion=$request->dsceribtion;
        
        $imgname = time().'.'.$request->img->extension();  
        $request->img->move(public_path('uploads/catge'), $imgname);
        $catgoery->img=$imgname;

        $catgoery->save();
        return redirect()->back()->with('success', 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
