<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\couses;
use App\subCategoery;

use App\getvideo;
use Illuminate\Support\Facades\Auth;

class apiCoursesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $couses=couses::all();
        return response()->json(['couses'=>$couses,'message'=>'success'],200);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
 {

    $couses=couses::all()->where('category',$id);
    $collection = collect($couses);

    return response()->json(['couses'=> $collection,'message'=>'success'],200);

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function showcosw($id)
    {

       $oin=subCategoery::find($id);

       return response()->json(['oin'=> $oin,'message'=>'success'],200);

           //
       }
       public function Join($id)
       {

          $oin=subCategoery::find($id);
          $oin->numcoursebuy+=1;
          $oin->save();
          $get=new getvideo();
          $get->user_id=Auth::User()->id;
          $get->couse_id=$id;
          $get->enrro=1;
          $get->save();



          return response()->json(['message'=>'success'],200);

              //
          }
}
