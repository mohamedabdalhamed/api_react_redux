<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('imges');
            $table->string('videos');
            $table->integer('numberOfVideo');
            $table->text('describtion');
            $table->integer('category')->default(0);
            $table->integer('numgetcourses')->default(0);
            $table->integer('age');
            


            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couses');
    }
}
