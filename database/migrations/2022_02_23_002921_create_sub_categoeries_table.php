<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCategoeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_categoeries', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->integer('numberCategoery_id');
            $table->string('imges');
            $table->integer('numcoursebuy')->default(0);

            // $table->foreign('numberCategoery_id')->references('id')->on('catgoeries')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_categoeries');
    }
}
